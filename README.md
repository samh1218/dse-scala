# Dynamic Story Engine (DSE)
#### Author : Samuel Huh
### What is it?
DSE is an engine centered around a powerful AI that generates compelling and powerful story dynamically (i.e. on the spot).

#### Progress
##### As of 09/07/2017
- (COMPLETED) Added GUI
- (COMPLETED) Added 'Page' in Game Folder
- (INCOMPLETE) Speak() in Speaker
- (INCOMPLETE) Dialogue Simulation between two speakers
- (INCOMPLETE) Dialogue Simulation between the same speaker (monologue)
- (INCOMPLETE) EventCard
- (INCOMPLETE) AIDirector
- (INCOMPLETE) Training of AIDirector

##### As of 09/04/2017
Completed : 70% of Atomic Classes 
- Finished porting to Scala (35% Completed for Porting)
- Reponse Completed
- QueryMap Completed
- World Completed
- Scene Completed
- Actor Completed
- Behaviour Completed
- Update(), FixedUpdate(), Recurring Methods, etc
- Multithreading
- Etc.

NEED TO COMPLETE :
- EventCard
- Dialogue Simulation b/w 2+ Characters
- Bug Fixes
- AIDirector
- TensorFlow integration
- Graphs : Undirected & Directed
- Neural Net Training for AIDirector
