package core.containers.graphs
/*
 * Interface Class.  Implemented by either
 * Undirected or Directed Graph
 *
 *@param V the Type of vertices
 *@param E the type of edge labels
 *
 *@author Samuel Huh
 */

abstract class Graph[V] {

  // Retrieves number of vertices
  def size: Int

  // Retrieves number of edges
  def edges: Int

  // checks to see if the specified vertex key
  // already exists within the game
  def contains(vertex: V): Boolean

  // Adds the vertex to the graph
  def addVertex(vertex: V): Unit

  def removeVertex(vertex: V): Unit

  // Adds the undirected edge v-w to this graph.
  def connect(vertexOne:V, vertexTwo:V): Unit

  // Returns true if there is a path from vertexOne
  // to vertexTwo
  def isConnected(vertexOne: V, vertexTwo:V): Boolean

  // Remove the connection between v-w in this graph
  def removePath(vertexOne: V, vertexTwo: V): Unit

  def degree(vertex: V): Int
  /*
   * Returns a string representation of this graph.
   */
  override def toString: String

}
