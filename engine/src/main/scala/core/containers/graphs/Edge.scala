package core.containers.graphs

class Edge[V>:Null, T>:Null](vertexOne:V, vertexTwo: T) {
  protected var _vertexOne:V = vertexOne
  protected var _vertexTwo:T = vertexTwo

  def setVertexOne(vertex:V): Unit = _vertexOne = vertexOne
  def setVertexTwo(vertex:T): Unit = _vertexTwo = vertexTwo
  def getVertexOne: V = _vertexOne
  def getVertexTwo: T = _vertexTwo
  def delete(): Unit = {
    _vertexTwo = null
    _vertexOne = null
  }
}
