package core.containers.graphs

import core.baseobject.UObject

import scala.collection.mutable

class DirectedGraph[T>:UObject, E>:Edge[T, T]] extends Graph[T] {
  // String -> Hexadecimal (pre-determined mapping) -> V
  protected var _verticesTable:mutable.HashMap[T, mutable.HashMap[Int, E]]
          = new mutable.HashMap[T, mutable.HashMap[Int, E]]()

  // Number of Connections in the Graph
  protected var _numOfEdges:Int = 0

  // Adding Vertex in the Directed Graph
  override def addVertex(vertex: T): Unit = {
    if (vertex != null && !contains(vertex)) {
      _verticesTable.put(vertex, new mutable.HashMap[Int, E]())
    }
  }

  // Remove vertex in the Directed Graph
  // Removes all the connections to that vertex
  // or from that vertex
  override def removeVertex(vertex: T): Unit = {
    if (!contains(vertex)) return
    for (edgeTable <- _verticesTable.values) {
      if (edgeTable.contains(vertex.hashCode())) {
        edgeTable -= vertex.hashCode()
        _numOfEdges -= 1
      }
    }
    _numOfEdges -= _verticesTable(vertex).size
    _verticesTable(vertex).empty
    _verticesTable -= vertex
    /*
    for (node <- _verticesTable.values) {
      if (node.removePath(vertex)) _numOfEdges -= 1
    }
    val node = _verticesTable(vertex.hashCode())
    val numOfConnections = node.degrees
    _numOfEdges -= numOfConnections
    node.emptyAllPaths()
    _verticesTable -= vertex.hashCode()
    */
  }

  override def isConnected(vertexOne: T, vertexTwo: T): Boolean = {
    if (!contains(vertexOne) || !contains(vertexTwo))
      return false
    _verticesTable(vertexOne).contains(vertexTwo.hashCode())
  }

  // returns true if the vertex exists in the table
  override def contains(vertex: T): Boolean = {
    if (vertex == null) return false
    _verticesTable.contains(vertex)
  }

  // Connecting a path between vertexOne and vertexTwo. No path will be
  // created between vertexTwo and vertexOne
  override def connect(vertexOne: T, vertexTwo: T): Unit = {
    if (contains(vertexOne) && contains(vertexTwo)) {
      if (!vertexOne.equals(vertexTwo) && !isConnected(vertexOne, vertexTwo)) {
        _verticesTable(vertexOne).put(vertexTwo.hashCode(),
                       new E(vertexOne, vertexTwo))
        _numOfEdges += 1
      }
    }
  }

  // Remove path between vertexOne and vertexTwo
  override def removePath(vertexOne: T, vertexTwo: T): Unit = {
    if (contains(vertexOne) && contains(vertexTwo)) {
      if (_verticesTable(vertexOne).contains(vertexTwo.hashCode())) {
        _verticesTable(vertexOne) -= vertexTwo.hashCode()
        _numOfEdges -= 1
      }
/*      val nodeOne = _verticesTable(vertexOne.hashCode())
      if (nodeOne.removePath(vertexTwo)) _numOfEdges -= 1
*/
    }
  }

  // Return the number of paths moving from the specified vertex object
  override def degree(vertex: T): Int = {
    if (contains(vertex)) _verticesTable(vertex).size
    else -1
  }

  // Return the number of vertices in the graph
  override def size: Int = _verticesTable.size

  // Get # of connections in the graph
  override def edges: Int = _numOfEdges

  // Returns the name of the graph
  override def toString: String = this.getClass.getName
}

object DirectedGraph {
  def main(args: Array[String]): Unit = {
    // Initialize the values...
    val obj = new UObject()
    val obj2 = new UObject("Hello World")
    var directedGraph = new DirectedGraph()
    // Test #1 : Adding a UObject
    directedGraph.addVertex(obj)
    assert(directedGraph.size == 1)
    assert(directedGraph.degree(obj) == 0)
    assert(directedGraph.edges == 0)
    // Test #2 : Test if null is ignored in addVertex
    directedGraph.addVertex(null)
    assert(directedGraph.size == 1)
    assert(directedGraph.degree(obj) == 0)
    assert(directedGraph.edges == 0)
    assert(directedGraph.contains(obj))
    assert(!directedGraph.contains(null))
    // Test #3 : Test removeVertex
    directedGraph.removeVertex(obj)
    assert(directedGraph.size == 0)
    assert(directedGraph.degree(obj) < 0)
    assert(directedGraph.edges == 0)
    assert(!directedGraph.contains(obj))
    // Reset
    directedGraph.addVertex(obj)
    // Test #4 : Add a second object
    directedGraph.addVertex(obj2)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 0)
    assert(directedGraph.contains(obj))
    assert(directedGraph.contains(obj2))
    // Test #5 : Connect obj -> obj2
    directedGraph.connect(obj, obj2)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 1)
    assert(directedGraph.degree(obj) == 1)
    assert(directedGraph.degree(obj2) == 0)
    assert(directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
    // Test #6 : Connect obj2 -> obj
    directedGraph.connect(obj2, obj)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 2)
    assert(directedGraph.degree(obj) == 1)
    assert(directedGraph.degree(obj2) == 1)
    assert(directedGraph.isConnected(obj, obj2))
    assert(directedGraph.isConnected(obj2, obj))
    // Test #6 : Remove Vertex
    directedGraph.removeVertex(obj)
    assert(directedGraph.size == 1)
    assert(directedGraph.edges == 0)
    assert(directedGraph.degree(obj) < 0)
    assert(directedGraph.degree(obj2) == 0)
    assert(!directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
    // Reset -- add and connect the second element
    directedGraph.addVertex(obj)
    assert(directedGraph.size == 2)
    // Test #7 : Ignore duplicate element
    directedGraph.addVertex(obj2)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 0)
    assert(directedGraph.degree(obj) == 0)
    assert(directedGraph.degree(obj2) == 0)
    assert(!directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
    // Reset connections
    directedGraph.connect(obj, obj2)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 1)
    assert(directedGraph.degree(obj) == 1)
    assert(directedGraph.degree(obj2) == 0)
    assert(directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
    // Test #8 : Ignore duplicate connection (no cycle on same obj)
    directedGraph.connect(obj, obj)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 1)
    assert(directedGraph.degree(obj) == 1)
    assert(directedGraph.degree(obj2) == 0)
    assert(directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
    assert(!directedGraph.isConnected(obj, obj))
    // Test #9 : Ignore removing a non-existent connection
    directedGraph.removePath(obj2, obj)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 1)
    assert(directedGraph.degree(obj) == 1)
    assert(directedGraph.degree(obj2) == 0)
    assert(directedGraph.isConnected(obj, obj2))
    // Test #10 : Remove an existing path
    directedGraph.removePath(obj, obj2)
    assert(directedGraph.size == 2)
    assert(directedGraph.edges == 0)
    assert(directedGraph.degree(obj)  == 0)
    assert(directedGraph.degree(obj2) == 0)
    assert(!directedGraph.isConnected(obj, obj2))
    assert(!directedGraph.isConnected(obj2, obj))
  }
}