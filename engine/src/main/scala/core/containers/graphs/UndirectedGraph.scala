package core.containers.graphs

import core.baseobject.UObject

import scala.collection.mutable

class UndirectedGraph extends Graph[UObject] {
  // String -> Hexadecimal (pre-determined mapping) -> V
  private var _verticesTable:mutable.HashMap[UObject, mutable.HashMap[Int, Edge[UObject, UObject]]] = new mutable.HashMap[UObject, mutable.HashMap[Int, Edge[UObject, UObject]]]()

  // Number of Connections in the Graph
  private var _numOfEdges:Int = 0


  // Returns true if vertex exists in the table
  override def contains(vertex: UObject): Boolean = {
    if (vertex == null) return false
    _verticesTable.contains(vertex)

  }
  override def addVertex(vertex: UObject): Unit = {
    if (vertex != null && !contains(vertex)) {
      _verticesTable.put(vertex, new mutable.HashMap[Int, Edge[UObject, UObject]]())
    }
  }

  override def removeVertex(vertex: UObject): Unit = {
    if (contains(vertex)) {
      // Retrieve the Node using the passed 'vertex' value
      for (edge <- _verticesTable(vertex).values) {
        removePath(edge.getVertexTwo, vertex)
        edge.delete()
      }
      _verticesTable(vertex).empty
      _verticesTable -= vertex
      /*
      val node = _verticesTable(vertex.hashCode())
      for (vertexVal <- node.getConnectedElems) {
        val connectedNode = _verticesTable(vertexVal.hashCode())
        if (connectedNode.removePath(vertex)) _numOfEdges -= 1
      }
      node.emptyAllPaths()
      _verticesTable -= node.getValue.hashCode()
      */
    }
  }

  override def connect(vertexOne: UObject, vertexTwo: UObject): Unit = {
    if (vertexOne == null || vertexTwo == null) return
    if (!contains(vertexOne)) addVertex(vertexOne)
    if (!contains(vertexTwo)) addVertex(vertexTwo)
    if (isConnected(vertexOne, vertexTwo)) return
    var map = _verticesTable(vertexOne)
    map += (vertexTwo.hashCode() -> new Edge[UObject, UObject](vertexOne, vertexTwo))
    _verticesTable(vertexOne).put(vertexTwo.hashCode(), new Edge[UObject, UObject](vertexOne, vertexTwo))
    _verticesTable(vertexTwo).put(vertexOne.hashCode(), new Edge[UObject, UObject](vertexTwo, vertexOne))

/*
      val nodeOne = _verticesTable(vertexOne.hashCode())
      val nodeTwo = _verticesTable(vertexTwo.hashCode())
      nodeOne.addConnection(vertexTwo)
      nodeTwo.addConnection(vertexOne)
*/
    _numOfEdges += 1
  }

  override def isConnected(vertexOne: _root_.core.baseobject.UObject, vertexTwo: _root_.core.baseobject.UObject): Boolean = {
    if (contains(vertexOne) && contains(vertexTwo))
      _verticesTable(vertexOne).contains(vertexTwo.hashCode())
    else false
  }

  override def removePath(vertexOne: UObject, vertexTwo: UObject): Unit = {
    if (!contains(vertexOne) || !contains(vertexTwo)) return
    if (!isConnected(vertexOne, vertexTwo)) return
    _verticesTable(vertexOne) -= vertexTwo.hashCode()
    _verticesTable(vertexTwo) -= vertexOne.hashCode()
    _numOfEdges -= 1
  }

  override def degree(vertex : UObject): Int = {
    if (contains(vertex)) _verticesTable(vertex).size
    else -1
  }

  override def size: Int = _verticesTable.size

  // Get number of connections in the graph
  override def edges: Int = _numOfEdges

  // Returns the name of the graph
  override def toString: String = this.getClass.getName

}

object UndirectedGraph {
  def main(args: Array[String]): Unit = {
    // Instantiation
    val obj = new UObject("Bye Bye")
    val obj2 = new UObject("Hello World!")
    val graph = new UndirectedGraph
    // Test #1 : Check if we can add the object
    graph.addVertex(obj)
    assert(graph.size == 1)
    assert(graph.contains(obj))
    // Test #2 : Check 'contain' function on object that
    // is not already added
    assert(!graph.contains(obj2))
    // Test #3 : Check if removing an object works
    graph.removeVertex(obj)
    assert(graph.size == 0)
    assert(graph.edges == 0)
    assert(!graph.contains(obj))
    assert(graph.degree(obj) == -1)
    // Add again to reset
    graph.addVertex(obj)
    assert(graph.size == 1)
    assert(graph.contains(obj))
    assert(graph.degree(obj) == 0)
    // Test #4 : Test if null is ignored for addVertex
    graph.addVertex(null)
    assert(graph.size == 1)
    assert(graph.contains(obj))
    assert(!graph.contains(null))
    // Test #5 : Test if null is ignored for removeVertex
    graph.removeVertex(null)
    assert(graph.size == 1)
    assert(graph.contains(obj))
    // Test #6 : Test if duplicate element is not added
    graph.addVertex(obj2)
    assert(graph.size == 2)
    assert(graph.contains(obj))
    assert(graph.contains(obj2))
    assert(graph.degree(obj)  == 0)
    assert(graph.degree(obj2) == 0)
    graph.connect(obj, obj2)
    assert(graph.degree(obj)  == 1)
    assert(graph.degree(obj2) == 1)
    assert(graph.edges == 1)
    assert(graph.isConnected(obj, obj2))
    assert(graph.isConnected(obj2, obj))
    // Test #7 : Test if removing element will...
    graph.removeVertex(obj)
    // First : Reduce the size of vertices in the graph by 2
    assert(graph.size == 1)
    assert(!graph.contains(obj))
    assert(graph.contains(obj2))
    // Second : Number of edges will be reduced to 0
    assert(graph.edges == 0)
    // Third : Number of degrees of the remaining object will be 0
    assert(graph.degree(obj2) == 0)
    // Fourth : No connection between obj and obj2
    assert(!graph.isConnected(obj, obj2))
    assert(!graph.isConnected(obj2, obj))
  }
}
