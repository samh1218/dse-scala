package core.baseobject

import java.io.FileWriter
import java.util.Properties
import java.util.zip.CRC32

import core.utilities.UConfigReader

class UObject(_name:String = "UObject", _bDontDeleteLoadLevel:Boolean = false) {

  // Name of the Object. Components share the same name with
  // the Actor and all attached components
  protected var name:String = _name

  // When true, Object remains when loading a new level
  // Default : false
  protected var bDontDeleteLoadLevel:Boolean = _bDontDeleteLoadLevel

  // begins the loading process for the object
  // used for loading the file from Disk
  def load(properties: Properties): Unit = {
    name = properties.getProperty("name")
    bDontDeleteLoadLevel = properties.getProperty("bDontDeleteLoadLevel").toBoolean
  }

  protected def postLoad(): Unit = {

  }


  protected def init(): Unit = {

  }


  /*
   * Constructs a hashtable that stores all variables of this Object.
   * Used for finding data quickly from this object, such as when choosing
   * a dialogue
   */
  def buildDataTable() : Map[String, Any] = {
    var datatable:Map[String, Any] = Map()
    for (field <- this.getClass.getDeclaredFields) {
      println(field.getName)
      println(field.get(this))
      datatable += (field.getName -> field.get(this))
    }
    datatable
  }

  def setName(_name:String) {
    this.name = _name
  }

  override def hashCode(): Int = {
    val cRC32 = new CRC32
    cRC32.update(_name.getBytes())
    cRC32.getValue.toInt
  }
  def dontDeleteLoadLevel() : Boolean = bDontDeleteLoadLevel

  def getInstanceId: Int = hashCode()

  protected def sleep(time:Float): Unit = {
    val startTime:Long = System.nanoTime()
    var elapsedTime:Float = (System.nanoTime() - startTime).toFloat / 1000000000.0f
    while (elapsedTime < time) {
      elapsedTime = (System.nanoTime() - startTime).toFloat / 1000000000.0f
    }
    //Thread.sleep(time)
  }

  override def toString: String = name

  // Saves the Object into the Objects' Properties file
  def save(): Unit = {
    val config:Properties = new Properties()
    val path = System.getProperty("user.dir") + UConfigReader.getPropertyFile("paths.properties").getProperty("path.object")
    config.setProperty(name + "." +"name", name)
    config.setProperty(name + "." + "bDontDeleteLoadLevel", bDontDeleteLoadLevel.toString)
    config.store(new FileWriter(path, true), name)
  }
}

// Static Object a.k.a. 'Companion Object'
object UObject {
  def setDontDeleteLoadLevel(baseObj:UObject, _bDontDeleteLoadLevel:Boolean): Unit = {
    baseObj.bDontDeleteLoadLevel = _bDontDeleteLoadLevel
  }

  def main(args: Array[String]): Unit = {
    // Unit Testing #1 : Reflection
    val uObject = new UObject("uObject")
    uObject.buildDataTable()
  }
}