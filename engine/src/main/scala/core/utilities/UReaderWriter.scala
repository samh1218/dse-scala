package core.utilities

import java.io._

// Class responsible for reading and writing files.
object UReaderWriter {

  def writeLine(line:String, file: File, isAppend:Boolean = true): Unit = {
    new BufferedWriter(new FileWriter(file, isAppend)).write(line + "\n")
  }

  def copyFile(inFile: File, outFile: File): Unit = {
    if (recreateFile(outFile) == null) return
    val br:BufferedReader = new BufferedReader(new FileReader(inFile))
    var line:String = ""
    while ({
      line = br.readLine()
      line != null
    }) {
      writeLine(line, outFile)
    }
  }

  def recreateFile(file: File): File = {
    val path:String = file.getAbsolutePath
    if (file.exists()) file.delete()
    createFile(path)
  }

  def createFile(path:String): File = {
    var file:File = new File(path)

    if (!file.exists()) {
      System.out.println("Creating Directory: " + file.getParent)
      file.mkdir()
    }
    file
  }
}
