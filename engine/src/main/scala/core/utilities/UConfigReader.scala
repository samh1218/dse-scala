package core.utilities

import java.io.{FileInputStream, InputStream}
import java.util.Properties

object UConfigReader {
  def getPropertyFile(fileName:String): Properties = {
    val path = System.getProperty("user.dir") + "/engine/src/main/resources/config/"
    val prop = new Properties()
    val inputStream:InputStream = new FileInputStream(path + fileName)
    prop.load(inputStream)
    prop
  }
}
