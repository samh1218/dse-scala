package core.tags

object EHideFlag extends Enumeration{
    val NONE                      // Normal, visible object. Default
//    HIDE_IN_HIERARCHY,          // Object will not appear in hierarchy (IGNORE)
//    HIDE_IN_INSPECTOR,          // Not possible to view it in the Inspector
//    DONT_SAVE_IN_EDITOR,        // Object not to be saved to the scene in Editor
//    NOT_EDITABLE,               // Not editable in the inspector
//    DONT_SAVE_IN_BUILD,         // Not saved when building a player
//    DONT_UNLOAD_UNUSED_ASSET,   // Not unloaded by Resources.UnloadUnusedAssets
//    DONT_SAVE,                  // Not saved to the scene. Will not be destroyed when new scene loaded.
//    HIDE_AND_DONT_SAVE          // Not shown in hierarchy, not saved to scenes, and not unloaded.
      = Value
}
