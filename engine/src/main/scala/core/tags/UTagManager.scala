package core.tags

import scala.collection.mutable

object UTagManager {
  private var tags:mutable.HashSet[String] = new mutable.HashSet[String]()

  /*
   * Adds the tag in the manager
   * if the tag does not already exist
   */
  def add(tag:String): Unit = {
    if (tag != null) tags.add(tag)
  }

  /*
   * Returns true if the tag exists in
   * the TagManager. Returns false, otherwise
   */
  def contains(tag:String): Boolean = tags.contains(tag)

  /*
   * Attempts to remove the tag, if the tag
   * exists in the TagManager.
   *
   * Returns true if successful; False, otherwise
   */
  def remove(tag:String): Boolean = {
    if (contains(tag)) {
      tags.remove(tag)
      return true
    }
    false
  }

  def toArray: Array[String] = {
    val tagArray:Array[String] = new Array[String](tags.size)
    var i:Int = 0
    for ( tag <- tags) {
      tagArray.update(i, tag)
      i += 1
    }
    tagArray
  }
}
