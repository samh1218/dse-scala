package core.tags

object ESendMessageOptions extends Enumeration {
  val REQUIRE_RECEIVER,     // Receiver is required for sendMessage
      DONT_REQUIRE_RECEIVER // No Receiver is required for sendMessage
  = Value
}
