package engine.objects.characters

import core.containers.graphs.{DirectedGraph, Edge}
import engine.objects.ACharacter

/*
 * Relationship outlining the nature of the relationship
 * between characters
 *
 */
class URelationshipGraph[A>:ACharacter] extends DirectedGraph[A,URelationEdge[A]] {
  var DEFAULT_AFFINITY = 50
  override def connect(vertexOne: A, vertexTwo: A): Unit = {
    super.connect(vertexOne, vertexTwo)
    _verticesTable(vertexOne)(vertexTwo.hashCode()).setAffinity(DEFAULT_AFFINITY)
  }
}
