package engine.objects.characters

import core.containers.graphs.Edge
import engine.objects.ACharacter

class URelationEdge[A>:ACharacter](vertexOne: A, vertexTwo: A)
  extends Edge[A, A](vertexOne, vertexTwo){
  /* How close the relationship is between vertexOne
      and vertexTwo */
  protected var _affinity:Int = 0
  def setAffinity(value:Int): Unit = _affinity = value
}
