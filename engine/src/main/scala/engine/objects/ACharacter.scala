package engine.objects

class ACharacter(name: String) extends AActor(name) {
  class PersonalityTraits {
    var CHARM:Int             = 5
    var COURAGE:Int           = 5
    var TENACITY:Int          = 5
    var EMPATHY:Int           = 5
    var LOYALTY:Int           = 5
    var AGGRESSION:Int        = 5
    var CURIOSITY:Int         = 5
    var IMAGINATION:Int       = 5
    var DECISIVENESS:Int      = 5
    var PATIENCE:Int          = 5
    var SELF_PRESERVATION:Int = 5
    var CRUELTY:Int           = 5
    var HUMILITY:Int          = 5
    var MEEKNESS:Int          = 5
    var COORDINATION:Int      = 5
    var VIVACITY:Int          = 5
    var CANDOR:Int            = 5
    var BULK_APPRECIATION:Int = 5
    var HUMOUR:Int            = 5
  }
  var traits = new PersonalityTraits

  /*
   * Decide whether to receive the trigger
   * or not. Score the strength the trigger
   * and determine whether or not the Character
   * should respond to that trigger.
   *
   *@param trigger
   */
  def receiveTrigger(): Boolean = {
    false
  }

  /*
   * Begins the Dialog process for this Character
   */
  def Speak(): Unit = {

  }

  override def buildDataTable(): Map[String, Any] = {
    super.buildDataTable()
  }
}

object ACharacter {
  def main(args: Array[String]): Unit = {

  }
}
