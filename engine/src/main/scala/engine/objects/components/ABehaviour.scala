package engine.objects.components

import java.util.concurrent.{ScheduledFuture, ScheduledThreadPoolExecutor, TimeUnit}

import engine.objects.AActor

class ABehaviour(_name:String, _componentTag:String = null, _owner:AActor, _fixedUpdateTime:Long = 5, _numOfThreads:Int = 5 ) extends ASwitchComponent(_name, _componentTag, _owner) {
  name         = _name
  componentTag = _componentTag
  owner        = _owner

  // The fixed rate (time in seconds, for interval)
  // for fixedUpdate function
  private val fixedUpdateTime:Long = _fixedUpdateTime

  // Number of threads used for updating functions
  private val numOfThreads = _numOfThreads

  private val repeatMethodThreadPool:ScheduledThreadPoolExecutor  = new ScheduledThreadPoolExecutor(_numOfThreads)

  // Repeating method table
  // String : the name of the recurring method
  // RunnableFuture : the task that requires updating
  private var repeatMethodTable:Map[String, ScheduledFuture[_]] = Map[String, ScheduledFuture[_]]()

  /*
   * Cancels all 'invoke()' calls on this Behaviour.
   * Basically all recurring method are cancelled when
   * this method is called
   */
  def cancelInvoke():Unit = {
    for (key <- repeatMethodTable.keySet) {
      cancelInvoke(key)
    }
  }

  /*
   * Same as above but filtered to only those methods
   * with the matching names
   */
  def cancelInvoke(methodName:String): Boolean = {
    val methodThread = repeatMethodTable.get(methodName).get
    if (methodThread == true) {
      if (!methodThread.isCancelled) {
        methodThread.cancel(false)
        return true
      }
    }
    false
  }

  /*
   * Invokes the method 'methodName' in 'time' seconds
   * then repeatedly every 'repeatRate' seconds
   * Only cancelled when owning actor is destroyed
   * or cancelled explicitly by calling the 'cancelInvoke'
   *
   * As of 08/31/2017 : DO NOT USE!!
   */
  def invokeRepeating(methodName:String, any: Any=null, time:Long = 1, repeatRate:Long = 1): Unit = {
    val method = getClass.getMethod(methodName)
    if (method == null) return

    val threadTask = new Runnable {
      override def run(): Unit ={
        invoke(methodName, any)
      }
    }

    val task:ScheduledFuture[_] = repeatMethodThreadPool.scheduleAtFixedRate(threadTask, time, repeatRate, TimeUnit.SECONDS)
    repeatMethodTable += (methodName -> task)

  }
  def invoke(methodName:String, any: Any=null): Unit ={
    val method = getClass.getMethod(methodName)
    if (method != null) {
      if (any != null) method.invoke(this) else method.invoke(this, any.asInstanceOf[AnyRef])
    }
  }

  //----------//
  // MESSAGES //
  //----------//

  /*
   * Called when the Behaviour instance is being loaded.
   * 'Awake' is used to initialize any variables or states
   * before the game starts. Method is called only once during
   * the lifetime of this object. Awake() is called after all
   * objects are initialized so you can safely speak to other
   * objects or query
   *
   * Use Awake() to set up references between scripts and use
   * 'Start' to pass any information back and forth
   */
  def awake(): Unit = {
    // Add the fixedUpdate(), Update(), and lateUpdate()
    // into the recurring methods table
    val fixedMethodName      = "fixedUpdate"
    val updateMethodName     = "update"
    val lateUpdateMethodName = "lateUpdate"
    invokeRepeating(fixedMethodName, null, 1, fixedUpdateTime)
    invokeRepeating(updateMethodName)
    invokeRepeating(lateUpdateMethodName)

  }

  /*
   * This function is called every fixed framerate frame for
   * dealing with methods requiring being called at fixed
   * interval
   */
  def fixedUpdate(): Unit = {

  }

  def update(): Unit = {

  }

  /*
   * Called every frame. Called after all 'Update' functions
   * have been called
   */
  def lateUpdate(): Unit = {

  }

  /*
   * Called when player gets or loses focus (ex. Alt-tabbing)
   * Sent to all actors (by the world). Will be run every frame
   * until player resumes
   */
  def onApplicationFocus(hasFocus:Boolean): Unit = {
    if (!hasFocus) {
      for (task <- repeatMethodTable.values) {
        task.wait()
      }
    }else {
      for (task <- repeatMethodTable.values) {
        task.notify()
      }
    }
  }

}
