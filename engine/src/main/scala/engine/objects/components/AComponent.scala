package engine.objects.components

import core.baseobject.UObject
import core.tags.UTagManager
import engine.objects.AActor
import core.tags.ESendMessageOptions

class AComponent(_name:String, _componentTag:String = null, _owner:AActor ) extends UObject {

  /*************************************/
  /* Constructors and Member Variables */
  /*************************************/

  // 'Actor' that owns this component.
  // A component is always attached (a.k.a. owned)
  // to an 'Actor'
  protected var owner:AActor = _owner
  name = _name
  protected var componentTag:String = _componentTag

  // Add the componentTag to the TagManager
  UTagManager.add(componentTag)
  //----------------//
  // PUBLIC METHODS //
  //----------------//

  def getOwner: AActor = owner

  override def buildDataTable(): Map[String, Any] = {
    var datatable = super.buildDataTable()
    datatable += ("owner" -> owner)
    datatable += ("componentTag" -> componentTag)
    datatable
  }

  /*
   * Calls the method named 'methodName' on every 'Behaviour'
   * in the owner of this component and any of its children.
   * All parameters except the 'methodName' are in their
   * default value.
   *
   *@param methodName the method to find and call
   */
  def broadcastMessage(methodName:String): Unit = {
    if (methodName != null) {
      broadcastMessage(methodName, null, ESendMessageOptions.DONT_REQUIRE_RECEIVER)
    }
  }

  def broadcastMessage(methodName:String, any: Any, sendMessageOptions: ESendMessageOptions.Value): Unit = {
    if (owner == null) return
    owner.broadcastMessage(methodName, any, sendMessageOptions)
  }

  def sendMessage(methodName:String, any: Any, sendMessageOptions: ESendMessageOptions.Value): Unit = {
    if (owner == null) return
    owner.sendMessage(methodName, any, sendMessageOptions)
  }
  def compareTag(tag:String): Boolean = componentTag == tag

  def getComponent(classTag : Class[_] ): AComponent = getComponentFromActor(classTag, owner)

  def getComponentRecursive(classTag: Class[_] ): AComponent = {
    var component:AComponent = getComponent(classTag)
    if (component == null) {
      for (actor <- owner.getChildrenList) {
        component = getComponentFromActor(classTag, actor)
        if (component != null) return component
      }
    }
    component
  }

  def getComponentInParent(classTag: Class[_] ): AComponent = {
    var component:AComponent = getComponent(classTag)
    if (component == null) {
      for (actor <- owner.getParentList) {
        component = getComponentFromActor(classTag, actor)
        if (component != null) return component
      }
    }
    component
  }

  private def getComponentFromActor(classTag: Class[_], actor: AActor): AComponent = {
    for (component <- actor.getComponents) {
      if (component.getClass.equals(classTag)) {
        return component
      }
    }
    null
  }
}
