package engine.objects.components

import engine.objects.AActor

class ASwitchComponent(_name:String, _componentTag:String = null, _owner:AActor, _enabled:Boolean = true ) extends AComponent(_name, _componentTag, _owner) {
  owner = _owner
  name  = _name
  componentTag = _componentTag

  // If true, component able to update
  // DEFAULT : true
  var enabled  = _enabled

  def isEnabled: Boolean = this.enabled

  def isActive: Boolean = {
    if (getOwner == null) return false
    isEnabled && getOwner.isActive
  }

  def setEnabled(enabled:Boolean): Unit = {
    this.enabled = enabled
  }

}
