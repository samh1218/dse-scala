package engine.objects

import java.util.Properties

import core.baseobject.UObject
import core.tags.ESendMessageOptions
import engine.objects.components.{ABehaviour, AComponent}
import engine.scene.{UScene, UWorld}

import scala.collection.mutable.ArrayBuffer

class AActor(_name:String) extends UObject{

  name                          = _name
  protected var componentList   = new ArrayBuffer[AComponent]()
  protected var parentList      = new ArrayBuffer[AActor]()
  protected var childrenList    = new ArrayBuffer[AActor]()
  protected var bActiveSelf     = true
  protected var actorTag:String = _
  protected var scene:UScene    = _

  /*
   * Constructs the Actor in the defined 'scene' at the defined 'time'
   *
   * For more information, read the 'Actor Lifecycle' in the documentation
   *
   *@param scene the scene (a.k.a. 'level') this Actor will be built in
   *@param time the time, in seconds, before the spawning happens (DEFAULT : 0)
   *@param owner (optional) the owner of this actor
   */
  def spawn(scene: UScene, time: Float = 0, owner : AActor = null): Unit = {
    sleep(time)
    if (owner != null && !parentList.contains(owner))
      parentList += owner
    this.scene = scene
    this.scene.add(name, this)
    postSpawnInitialize()
    onConstruction()
    preInitializeComponents()
    initializeComponents()
    postInitializeComponents()
    init()
  }

  // called after spawn
  protected def postSpawnInitialize(): Unit = {

  }

  // called after spawning. Constructs the body / data of the actor
  // when placed in the scene
  protected def onConstruction(): Unit = {

  }

  // loads the first aspects of the Object from the disk. (Save file)
  override def load(properties: Properties): Unit = {
    super.load(properties)
    preInitializeComponents()
    initializeComponents()
    postInitializeComponents()
    init()
  }

  // called after load()
  protected override def postLoad(): Unit = {

  }

  def destroy(): Unit ={
    beginDestroy()
    endDestroy()
  }
  protected def beginDestroy(): Unit = {

  }

  protected def endDestroy(): Unit = {

  }

  // called during the first phase of loading the components
  // i.e. calling the components' constructors
  protected def preInitializeComponents(): Unit = {

  }

  // calls the initialization function of the components
  protected def initializeComponents(): Unit = {

  }

  // calls the post initialization function of the components
  protected def postInitializeComponents(): Unit = {

  }

  // Initializes the object
  protected override def init(): Unit = {

  }

  /*
   * Adds the component with the defined type and defined name (optional)
   */
  def addComponent(classTag: Class[_], componentName:String = null ): AComponent = {
    var component:AComponent = getComponent(classTag)
    if (component != null) return component

    var newComponentName = componentName
    if (newComponentName == null) newComponentName = classTag.toString
    component = new AComponent(newComponentName, getActorTag, this)
    componentList += component
    component
  }

  /*
   * Returns true if a component with the defined type
   * exists in the Actor. Return false, otherwise.
   *
   *@param type the class type of the component in question
   */
  def componentExist(classTag: Class[_] ): Boolean = getComponent(classTag) != null

  def broadcastMessage(methodName:String, any: Any, sendMessageOptions: ESendMessageOptions.Value): Unit = {
    if (AActor.sendMessage(this, methodName, any)) return

    for (actor: AActor <- childrenList) {
      if (actor.isActive && AActor.sendMessage(actor, methodName, any))
        return
    }
  }

  def sendMessage(methodName:String, any: Any, sendMessageOptions: ESendMessageOptions.Value): Unit = {
    val sendMessageSuccess = AActor.sendMessage(this, methodName, any)
    if (sendMessageSuccess && sendMessageOptions.equals(ESendMessageOptions.REQUIRE_RECEIVER))
      throw new IllegalArgumentException("No method : " + methodName + " found in Actor " + toString())
  }


  def getComponent(classTag: Class[_] ): AComponent = getComponent(classTag, includeInactive = false)

  def getComponentRecursive(classTag: Class[_] , includeInactive:Boolean): AComponent = {
    var component = getComponent(classTag, includeInactive)
    if (component != null) return component
    for (child <- childrenList) {
      component = child.getComponentRecursive(classTag, includeInactive)
      if (component != null) return component
    }
    component
  }

  def getComponentInParent(classTag: Class[_] ): AComponent = {
    var component = getComponent(classTag)
    if (component != null) return component
    for (parent <- parentList) {
      component = parent.getComponentInParent(classTag)
      if (component != null) return component
    }
    component
  }

  def getComponents(classTag: Class[_] ) : ArrayBuffer[AComponent] = {
    var components = new ArrayBuffer[AComponent]()
    for (component <- componentList) {
      if (component.getClass.eq(classTag))
        components += component
    }
    components
  }

  def getComponentsRecursive(classTag: Class[_] ) : ArrayBuffer[AComponent] = {
    var components = new ArrayBuffer[AComponent]()
    for (component <- getChildrenList) {
      components = components ++ component.getComponentsRecursive(classTag)
    }
    components
  }

  def getComponentsInParent(classTag: Class[_] ): ArrayBuffer[AComponent] = {
    var components = new ArrayBuffer[AComponent]()
    for (parent <- getParentList) {
      components = components ++ parent.getComponentsRecursive(classTag)
    }
    components
  }

  override def buildDataTable(): Map[String, Any] = super.buildDataTable()

  def compareTag(tag:String): Boolean = getActorTag.equals(tag)
  def getActorTag: String = actorTag
  def isActive: Boolean = bActiveSelf
  def getComponents: ArrayBuffer[AComponent] = componentList
  def getChildrenList : ArrayBuffer[AActor]  = childrenList
  def getParentList:    ArrayBuffer[AActor]  = parentList
  def getScene:                     UScene   = scene

  //------------------------------//
  // PROTECTED / PRIVATE METHODS  //
  //------------------------------//
  protected def getComponent(classTag: Class[_], includeInactive:Boolean ): AComponent = {
    if (!includeInactive && !bActiveSelf) return null
    for (component <- componentList) {
      if (component.getClass.eq(classTag))
        return component
    }
    null
  }
}

//-----------------//
// STATIC METHODS  //
//-----------------//
object AActor {
  /*
   * Attempts to invoke the method via reflection if the method
   * with the 'methodName' exists in the specified object.
   *
   *@param object the object that may possess a method with the methodName and parameters
   *@param methodName the name of the method we are attempting to discover
   *@param parameter OPTIONAL parameters that the method might contain
   */
  def sendMessage(actor: AActor, methodName: String, any: Any): Boolean = {
    if (!actor.isActive) return false
    for (component <- actor.getComponents) {
      if (component.isInstanceOf[ABehaviour]) {
        component.asInstanceOf[ABehaviour].invoke(methodName, any)
        return true
      }
    }
    false
  }

  // Finds the Actor (file) by name and returns it
  // Useful for automatically connecting references
  // to other objects at load time
  // WARNING : EXPENSIVE OPERATION! NOT RECOMMENDED
  //           TO BE USED EVERY FRAME
  def find(name:String): AActor = {
    for (scene <- UWorld.getScenes) {
      val actor = scene.findActorByName(name)
      if (actor != null) return actor
    }
    null
  }

  /*
   * Finds the actor in the World via the 'actorTag.'
   * Finds the first matching actor and returns it
   *@param actorTag the Tag to find the desired Actor
   *
   * WARNING : Expensive operation!
   */
  def findByTag(tag:String): AActor = {
    for (scene <- UWorld.getScenes) {
      val actor = scene.findActorByTag(tag)
      if (actor != null) return actor
    }
    null
  }

  /*
   * Finds all the actors with the matching tag in the World.
   * Returns an ArrayBuffer of matching actors
   *
   *@param actorTag the tag to find the desired actors
   *
   * WARNING : Very expensive operation (both space and time)
   */
  def findActorsByTag(tag:String): ArrayBuffer[AActor] = {
    var actors = new ArrayBuffer[AActor]()
    for (scene <- UWorld.getScenes) {
      actors = actors ++ scene.findActors(tag)
    }
    actors
  }


  def main(args: Array[String]): Unit ={
  }
}
