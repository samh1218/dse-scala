package engine.dialog

import java.io.File

import core.utilities.UConfigReader
import engine.parsers.ADialogParser

// Parses all the Narrative XML files
// and stores them into a hashtable
object ADialogTable {

  private var _dialogHashTable:Map[String,ADialogUnit] = Map[String, ADialogUnit]()

  def init(): Unit = {
    val path = System.getProperty("user.dir") +  UConfigReader.getPropertyFile("paths.properties").getProperty("path.dialog")
    println("Loading Dialog at : " + path)
    val dialogDir:File = new File(path)
    val files = dialogDir.listFiles()
    for (file <- files) {
      if (file.getName.endsWith(".xml")) {
        val dialogUnit = ADialogParser.readDialog(file)
        val fileName = file.getName.split("\\.")(0)
        _dialogHashTable += (fileName -> dialogUnit)
      }
    }
  }

  def size():Int = _dialogHashTable.size


  def main(args: Array[String]): Unit = {
    ADialogTable.init()
    assert(ADialogTable.size() == 1)
  }
}
