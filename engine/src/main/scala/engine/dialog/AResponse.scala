package engine.dialog

import engine.objects.{ACharacter, AActor}
import engine.scene.UWorld

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class AResponse(speaker:ACharacter) {
  private val _options = new ArrayBuffer[AResponseOption]()
  private val _speaker = speaker
  private var _maxNumOfApplyFacts = 0

  def run(queryMap: AQueryMap): Unit = {
    val response = selectHighestScoredOption(queryMap)
    println(response.getDialogLine)
    response.runApplyFacts()
  }

  protected def selectHighestScoredOption(queryMap: AQueryMap): AResponseOption = {
    var idxAndScore = Map[Int, Float]()
    var highestScoredOption:AResponseOption = _options(0)
    var highestScore:Float = score(highestScoredOption, queryMap)

    var idx:Int = 0
    for (option <- _options) {
      val optionScore = score(option, queryMap)
      if (optionScore > highestScore) {
        highestScore = optionScore
        highestScoredOption = option
      }
      if (optionScore == highestScore) {
        idxAndScore += (idx -> optionScore)
      }
      idx += 1
    }

    var bestOptions = new ArrayBuffer[AResponseOption]()
    for (key <- idxAndScore.keySet) {
      if (idxAndScore(key) == highestScore)
        bestOptions += _options(key)
    }

    val randomizer:Random = Random
    val bestOptionIdx = randomizer.nextInt(bestOptions.size - 1)

    bestOptions(bestOptionIdx)
  }

  def speakerIsAbleToSpeak(): Boolean =
      _speaker != null && _speaker.isActive

  /*
   * Scores the responseOption
   * Score is in the Range from 0 to 1, with 1 being the highest
   *
   * /--------------------/
   * As of 09/04/2017 : No complex computation for scoring
   * Currently, any complex mathematics only complicates but do
   * not notably improve the dialog selection process
   *
   * Scoring Method :
   * 1. if option has a triggerActor, has a 60% point
   * 2. # of 'facts' in applyFacts per response : higher
   *    makes up a higher point for the remaining 20%
   *
   * /---------------------/
   */
  def score(option:AResponseOption, queryMap: AQueryMap): Float = {
    var optionScore:Float = 0.2f

    if (option.getTriggerActor != null) {
      val target = queryMap.get(option.getTriggerActor.toString())
      val actor  = if (target != null) target.asInstanceOf[AActor] else null
      if (actor != null && actor.isActive)
        optionScore += 0.6f
      else return optionScore
    }

    if (option.getApplyFacts.nonEmpty) {
      var ratio:Float =
        option.getApplyFacts.size.asInstanceOf[Float] / _maxNumOfApplyFacts.asInstanceOf[Float]
      ratio *= 0.2f
      optionScore += ratio
    }

    optionScore
  }

  def add(option:AResponseOption): Unit = {
    if (option != null) {
      _options += option
    }
    if (option.getApplyFacts.size > _maxNumOfApplyFacts) {
      _maxNumOfApplyFacts = option.getApplyFacts.size
    }
  }
  def remove(option:AResponseOption): Unit = {
    _options -= option
    findMaxNumOfApplyFacts()
  }

  protected def findMaxNumOfApplyFacts(): Unit = {
    _maxNumOfApplyFacts = 0
    for (option <- _options) {
      val numOfApplyFacts = option.getApplyFacts.size
      if (numOfApplyFacts > _maxNumOfApplyFacts)
        _maxNumOfApplyFacts = numOfApplyFacts
    }
  }
}

/*
 *@param dialogLine The line to speak
 *@param triggerActor The potential follow-up speaker, to continue the
 * conversation
 */
class AResponseOption(dialogLine:String, triggerActor:ACharacter = null) {
  private val _dialogLine:String      = dialogLine
  private val _triggerActor:ACharacter = triggerActor
  private var _applyFacts:Map[String, Any] = Map[String,Any]()

  def getDialogLine: String = _dialogLine

  def getTriggerActor: ACharacter = _triggerActor

  def setApplyFacts(applyFacts:Map[String, Any]): Unit = {
    _applyFacts = applyFacts
  }

  def addApplyFact(key:String, value:Any): Unit = {
    _applyFacts += (key->value)
  }

  def getApplyFacts: Map[String, Any] = _applyFacts

  def runApplyFacts(): Unit ={
    for (key:String <- _applyFacts.keySet) {
      UWorld.update(key, _applyFacts(key))
    }
  }
}
