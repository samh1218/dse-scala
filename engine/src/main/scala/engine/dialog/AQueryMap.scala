package engine.dialog

import core.baseobject.UObject
import engine.objects.ACharacter
import engine.scene.{UScene, UWorld}

/*
 * Data storage built during the Character.Speak()
 * in order to 1.) decide whether or not to accept
 * the trigger and 2.) score the best dialogue to speak,
 * once accepted.
 *
 *
 * As of 08/25/2017:
 * The QueryMap's HashTable consists of
 * - World's data is implicitly 'added' (a.k.a. referred)
 *   when using the QueryMap, to allow non-duplicates.
 * - Add the Scene where the Speaker is located
 * - Add the Speaker him/herself.
 */
class AQueryMap(owner:ACharacter) {
  private var _queryMap:Map[String, Any] = Map()
  private var _owner    = owner
  _queryMap = _queryMap ++ UWorld.buildDataTable()

  def add(key:String, any: Any): Unit = {
    _queryMap += (key -> any)
  }

  def add(key:String, baseObject: UObject): Unit ={
    _queryMap = _queryMap ++ baseObject.buildDataTable()
  }

  /*
   * Retrieves the Object's key from the QueryMap
   */
  def get(key:String): Any = _queryMap(key)

  /*
   *
   */
  def getOwner: ACharacter = _owner

  /*
   *
   */
  def getScene: UScene = {
    if (_owner != null)
      return _owner.getScene
    null
  }

  /*
   * Removes the element at the specified key
   * and returns that deleted element.
   *
   * If no element exists at the key, return
   * null.
   */
  def remove(key:String): Any = UWorld.remove(key)

  def getClass(key:String): Class[_] = UWorld.getClass(key)
}

object AQueryMap {
  def main(args: Array[String]): Unit = {
    // Setting up the variables needed for testing
    UWorld.addData("onNewGame", true)
    UWorld.addScene(new UScene("demoScene"))
    UWorld.addData("narrator", new ACharacter("narrator"))

    var query:AQueryMap = new AQueryMap(UWorld.get("narrator").asInstanceOf[ACharacter])

  }
}
