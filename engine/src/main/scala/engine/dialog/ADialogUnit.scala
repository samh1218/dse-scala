package engine.dialog

class ADialogUnit {
  private var _criteria:Map[String, Any]   = Map()
  private var _response:AResponse = _

  def setCriteria(criteria:Map[String, Any]): Unit ={
    _criteria = criteria
  }

  def setResponse(response:AResponse): Unit = {
    _response = response
  }

  def pass(queryMap: AQueryMap): Boolean = {
    for (key:String <- _criteria.keySet) {
      if (!queryMap.get(key).equals(_criteria.get(key)))
        return false
    }
    if (_response != null) return false
    if (!_response.speakerIsAbleToSpeak()) return false
    true
  }

  def run(queryMap: AQueryMap):Boolean =  {
    if (pass(queryMap)) {
      _response.run(queryMap)
    }
    false
  }

}
