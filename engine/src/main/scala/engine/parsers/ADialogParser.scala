package engine.parsers

import java.io.File
import java.util

import core.utilities.UConfigReader

import collection.JavaConversions._
import engine.dialog.{ADialogUnit, AResponse, AResponseOption}
import engine.objects.ACharacter
import engine.scene.UWorld
import org.dom4j.{Document, DocumentException, Element, Node}
import org.dom4j.io.SAXReader

object ADialogParser {
  def readDialog(file: File): ADialogUnit = {
    var dialogUnit:ADialogUnit = new ADialogUnit()
    try {
      var criteriaTable:Map[String, Any]  = Map()

      val reader:SAXReader  = new SAXReader()
      val document:Document = reader.read(file)
      val root:Element = document.getRootElement

      // Read the Criteria
      val criteriaNode:Node = root.selectSingleNode("/dialog/criteria")
      criteriaTable = readGroup(criteriaNode, "criterion")

      // Read the Response
      var responseFileName:String = root.selectSingleNode("/dialog/response").getStringValue + ".xml"
      responseFileName = System.getProperty("user.dir") +
                         UConfigReader.getPropertyFile("paths.properties").getProperty("path.response") +
                         responseFileName
      val response:AResponse = readResponse(new File(responseFileName))

      println(criteriaTable.toString())

      dialogUnit.setCriteria(criteriaTable)
      dialogUnit.setResponse(response)

    } catch {
      case e:DocumentException =>
        println("Document Exception")
    }
    dialogUnit
  }

  /*
   * Reads the Group from the XML file
   *@param node the node of the Group
   *@param nodeName the name of the node of the group
   *@param table the table to store the key-value pairs defined in the group
   */
  def readGroup(node:Node, nodeName:String): Map[String, Any] = {
    val childrenList:util.List[Node] = node.selectNodes(nodeName).asInstanceOf[util.List[Node]]
    var table:Map[String, Any] = Map[String, Any]()
    for (child:Node <- childrenList) {
      val key:String = child.selectSingleNode("key").getStringValue
      val value:Any = convertValueToCorrectType(key, child.selectSingleNode("value").getStringValue)
      if (value != null) table += (key -> value)
    }
    table
  }

  def readResponse(file:File): AResponse = {
    try {
      val reader:SAXReader = new SAXReader()
      val document:Document = reader.read(file)
      val root:Element = document.getRootElement
      val speaker:String = root.selectSingleNode("/response/speaker").getStringValue

      val response:AResponse = new AResponse(UWorld.get(speaker).asInstanceOf[ACharacter])

      val optionsNode:Node = root.selectSingleNode("/response/options")
      val optionsList:util.List[Node] = optionsNode.selectNodes("option").asInstanceOf[util.List[Node]]
      for (child:Node <- optionsList) {
        var applyFactsTable:Map[String, Any] = Map[String, Any]()
        val line:String = child.selectSingleNode("line").getStringValue
        val triggerActorNode = child.selectSingleNode("triggerActor")
        var triggerActor:ACharacter = null
        if (triggerActorNode != null) {
           triggerActor = UWorld.get(triggerActorNode.getStringValue).asInstanceOf[ACharacter]
        }
        val applyFactsNode = child.selectSingleNode("applyFacts")
        applyFactsTable = readGroup(applyFactsNode, "fact")

        var responseOption:AResponseOption = new AResponseOption(line, triggerActor)
        responseOption.setApplyFacts(applyFactsTable)
        response.add(responseOption)

        // --------------- //
        //    TEMPORARY    //
        // --------------- //
        println("Response Line : " + line)
        println(applyFactsTable.toString())

      }
      return response
    } catch {
      case e:DocumentException => {
        println(e.getMessage)
      }
    }
    null
  }

  private def convertValueToCorrectType(key:String, value:String): Any = {
    if (value == null || key == null){
      println("Invalid Key or Value : " + " [key] : " + key + " [value] : " + value)
      return null
    }
    val currentVal = UWorld.get(key)
    if (currentVal != null) {
      if (currentVal.isInstanceOf[String]) return value
      else if (currentVal.isInstanceOf[Byte]) return value.toByte
      else if (currentVal.isInstanceOf[Int]) return value.toInt
      else if (currentVal.isInstanceOf[Double]) return value.toDouble
      else if (currentVal.isInstanceOf[Float]) return value.toFloat
      else if (currentVal.isInstanceOf[Long]) return value.toLong
      else if (currentVal.isInstanceOf[Char]) return value.toCharArray
      else if (currentVal.isInstanceOf[Short]) return value.toShort
      else if (currentVal.isInstanceOf[Boolean]) return value.toBoolean
    }
    null
  }

  def main(args: Array[String]): Unit = {
    UWorld.addData("onNewGame", true)
    UWorld.addData("narrator", new ACharacter("narrator"))
    ADialogParser.readDialog(new File("./src/main/resources/narrative/testDialogue.xml"))
    print(UWorld.get("onNewGame").toString)
  }
}
