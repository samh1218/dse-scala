package engine.scene

import core.baseobject.UObject
import engine.objects.AActor

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class UScene(name:String) extends UObject(name) {
  private var _inSceneObjects:Map[String, UObject] = Map[String, UObject]()
  private var _datatable:Map[String, Any] = Map[String, Any]()

  def load(): Unit = {

  }
  def add(key:String, any: Any): Unit ={
    if (any.isInstanceOf[UObject])
      addInScene(key, any.asInstanceOf[UObject])
    else
      addInData(key, any)
  }

  def add(key:String, baseObject: UObject): Unit ={
    if (key == null || baseObject == null) return
    addInScene(key, baseObject)
  }

  /*
   * Adds the key-value(non-Primitive) pair into the
   * 'datatable' hashtable
   *
   * If key exists already, will replace the key-value pair
   * with the one specified
   *
   *@param key the key linked to the value
   *@param value the non-Primitive data to add into the table
   */
  def addInData(key:String, any: Any): Boolean = {
    if (any == null || key == null) return false
    if (_datatable.contains(key) )
      _datatable -= key
    _datatable += (any.toString -> any)
    true
  }


  protected def addInScene(key:String, baseObject: UObject): Boolean = {
    if (baseObject == null || key == null) return false
    if (_inSceneObjects.contains(key)) return false
    _inSceneObjects += (key -> baseObject)
    true
  }

  protected def addInScene(baseObject: UObject): Unit = {
    if (baseObject == null) return
    if (_inSceneObjects.contains(baseObject.toString())) return
    val key:String = baseObject.toString()
    _inSceneObjects += (key -> baseObject)
  }

  def containsKey(key:String): Boolean = {
    if (key == null) return false
    if (_inSceneObjects.contains(key)) return true
    if (_datatable.contains(key)) return true
    false
  }

  /*
   * Returns true if the scene contains the key-value
   * pair, as specified. Both the key and the value
   * has to exist and match.
   * Returns false, otherwise.
   *
   * Non-Recursive search
   */
  def contains(key:String, any: Any): Boolean = {
    if (key == null || any == null) return false
    if (_inSceneObjects.get(key).equals(any)) return true
    if (_datatable.get(key).equals(any)) return true
    false
  }

  /*
   * Check if defined non-Primitive variable exists
   * in the scene
   *@param any the Non-Primitive variable to check if exists in the scene
   *
   * WARNING : Expensive Operation!
   */
  def contains(any: Any): Boolean = {
    if (any == null) return false
    if (any.isInstanceOf[UObject])
    {
      val checkObject:UObject = any.asInstanceOf[UObject]
      for (baseObject:UObject <-_inSceneObjects.values) {
        if (baseObject.equals(checkObject)) return true
      }
    }
    for (compareVal:Any <- _datatable.values) {
      if (compareVal.equals(any)) return true
    }
    false
  }

  /*
   * Recursive check to see if key is contained in the Scene
   */
  def containsKeyRecursive(key:String) : Boolean = {
    if (containsKey(key)) return true
    for (baseObject <- _inSceneObjects.values) {
      if (baseObject.buildDataTable().contains(key))
        return true
    }
    false
  }

  /*
   * Checks to see if the key-value exists in the scene
   * Recursive
   *
   * WARNING : EXPENSIVE OPERATION
   */
  def containsRecursive(key:String, any: Any): Boolean = {
    if (contains(key, any)) return true
    for (baseObject:UObject <- _inSceneObjects.values) {
      val map = baseObject.buildDataTable()
      if ( map.contains(key) && map.get(key).equals(any))
        return true
    }
    false
  }

  /*
   * Check if defined non-Primitive variable exists
   * in the scene. Recursive operation i.e. goes through each baseObject
   * to see if the variable exists!
   *@param any the Non-Primitive variable to check if exists in the scene
   *
   * WARNING : Very Expensive Operation!
   */
  def containsRecursive(any: Any): Boolean = {
    if (contains(any)) return true
    for (baseObject <- _inSceneObjects.values)
    {
      for (baseObjVariables: Any <- baseObject.buildDataTable().values) {
        if (baseObjVariables.equals(any)) return true
      }
    }
    false
  }

  def remove(key:String): Any = {
    var deleteObj = removeInData(key)
    if (deleteObj == null)
      deleteObj = removeInScene(key)
    deleteObj
  }

  def remove(key:String, any: Any): Any = {
    var removedObj = removeInData(key)
    if (removedObj == null || removedObj.equals(any)) {
      removedObj = removeInData(key)
    }
    removedObj
  }

  protected def removeInData(key:String): Any = {
    val deleteVariable = _datatable(key)
    _datatable -= key
    deleteVariable
  }

  protected def removeInScene(key:String): Any = {
    val deleteVariable:Any = _inSceneObjects(key)
    _inSceneObjects -= key
    deleteVariable
  }

  def update(key:String, any: Any): Unit = {
    if (any.isInstanceOf[UObject])
      update(key, any.asInstanceOf[UObject])
    _datatable += (key -> any)
  }

  def update(key:String, newObject:UObject): Unit = {
    _inSceneObjects += (key -> newObject)
  }

  def find(key:String): Any = {
    var value:Any = findActorByName(key)
    if (value == null) {
      value = findActorByTag(key)
      if (value == null) {
        value = _datatable(key)
      }
    }
    value
  }

  def findActorByName(name:String): AActor = {
    if (_inSceneObjects(name).isInstanceOf[AActor])
      return _inSceneObjects(name).asInstanceOf[AActor]
    null
  }

  def findActorByTag(tag:String): AActor = {
    for (baseObject:UObject <- _inSceneObjects.values) {
      if (baseObject.isInstanceOf[AActor]) {
        val actor = baseObject.asInstanceOf[AActor]
        if (actor.compareTag(tag))
          return actor
      }
    }
    null
  }

  /*
   * Returns an ArrayBuffer of Actors with the
   * specified tag
   */
  def findActors(tag:String): ArrayBuffer[AActor] = {
    var arrayBuffer:ArrayBuffer[AActor] = ArrayBuffer()
    for (baseObject:UObject <- _inSceneObjects.values) {
      if (baseObject.isInstanceOf[AActor]) {
        val actor = baseObject.asInstanceOf[AActor]
        if (actor.getActorTag.equals(tag))
          arrayBuffer += actor
      }
    }
    arrayBuffer
  }

  override def buildDataTable(): Map[String, Any] = {
    var map:Map[String, Any] = super.buildDataTable()
    map = map ++ _inSceneObjects
    map = map ++ _datatable
    map
  }
}
