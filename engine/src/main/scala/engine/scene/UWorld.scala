package engine.scene

import core.baseobject.UObject

import scala.collection.mutable.ArrayBuffer

object UWorld extends UObject{
  name = "world"
  private var _scenes:Map[String, UScene] = Map()
  private var _datatable:Map[String, Any] = Map()


  def addData(key:String, any: Any): Unit = {
    _datatable += (key -> any)
  }

  def addScene(scene: UScene): Unit = {
    if (scene == null || scene.toString() == null) return
    if (_scenes.contains(scene.toString())) return
    _scenes += (scene.toString() -> scene)
  }

  def addInScene(sceneKey:String, objKey:String, any: Any): Unit = {
    if (sceneKey == null || objKey == null || any == null) return
    if (!_scenes.contains(sceneKey)) return
    _scenes(sceneKey).add(objKey, any)
  }

  def update(key:String, any: Any): Unit = {

    // Check if the key-value is stored in the world's 'datatable'
    if (_datatable.contains(key)) {
      _datatable += (key -> any)
      return
    }

    // Check if the key is for finding the scene
    if (_scenes.contains(key)) {
      _scenes += (key -> any.asInstanceOf[UScene])
      return
    }

    // Check to see if the key-value pair exists within
    // a scene
    for (scene <- _scenes.values) {
      if (scene.containsKey(key)) {
        scene.update(key, any)
        return
      }
    }
  }

  def getScenes: ArrayBuffer[UScene] = {
    var arrayBuffer = new ArrayBuffer[UScene]()
    for (scene <- _scenes.values) {
      arrayBuffer += scene
    }
    arrayBuffer
  }

  /*
   * Returns the type of the object, accessible
   * by the defined 'key'
   *
   *@param key the key of the Object
   */
  def getClass(key:String): Class[_] = {
    if (_scenes.contains(key)) return _scenes.get(key).getClass
    if (_datatable.contains(key)) return _datatable.get(key).getClass

    for (scene <- _scenes.values) {
      if (scene.findActorByName(key) != null)
        return scene.findActorByName(key).getClass
    }
    null
  }

  /*
   * Retrieves the object in the World via the key
   */
  def get(key:String): Any = {
    if (key == null) return null
    var value = getData(key)
    if (value == null) {
      if (_scenes.contains(key)) {
        value = _scenes(key)
      }

    }
    value
  }

  /*
   * Retrieves the value using the defined 'key'
   * in the World's datatable
   *
   *@param key the Object's key in the datatable hashtable.
   */
  protected def getData(key:String): Any = {
    if (_datatable.contains(key))
      return _datatable(key)
    null
  }

  protected def getDataFromScenes(key:String): Any = {
    for (scene <- _scenes.values) {
      val obj = scene.find(key)
      if (obj != null) {
        return obj
      }
    }
    null
  }

  def remove(key:String): Any = {
    var deleteObj = removeInData(key)
    if (deleteObj == null)
      deleteObj = removeFromScenes(key)
    deleteObj
  }

  def remove(key:String, any: Any): Any = {
    var deleteObj = removeInData(key, any)
    if (deleteObj == null) {
      deleteObj = removeFromScenes(key, any)
    }
    deleteObj
  }

  def removeAll(key:String, any: Any): Unit = {
    if (removeInData(key, any) == null)
      removeAllFromScenes(key, any)
  }

  /*
   * Removes the key-value pair, stored in
   * the World's datatable, if the pair exists
   */
  protected def removeInData(key:String): Any = {
    val deleteObj = _datatable(key)
    _datatable -= key
    deleteObj
  }

  /*
   * Deletes if only both the key-value exist
   */
  protected def removeInData(key:String, any: Any): Any = {
    if (_datatable.contains(key) && _datatable.get(key).equals(any)) {
      return removeInData(key)
    }
    null
  }

  /*
   * Removes the key-value pair from ALL scenes
   *@param key the key to find the value to remove from the Scene
   */
  protected def removeAllFromScenes(key:String): Unit = {
    for (scene <- _scenes.values) {
      if (scene.containsKey(key)) {
        scene.remove(key)
      }
    }
  }

  /*
   * Removes the key-value pair for the FIRST MATCHING scene
   */
  protected def removeFromScenes(key:String): Any = {
    for (scene <- _scenes.values) {
      if (scene.containsKey(key)) {
        return scene.remove(key)
      }
    }
    null
  }

  /*
   * Removes only if both the key and value matches.
   * Removes from the first MATCHING scene.
   *@param key the key to match
   *@param any the value to match
   */
  protected def removeFromScenes(key:String, any: Any): Any = {
    for (scene <- _scenes.values) {
      if (scene.contains(any)) {
        return scene.remove(key, any)
      }
    }
    null
  }

  /*
   * Removes only if both the key and value matches.
   * Removes from ALL scenes
   *@param key the key to match
   *@param any the value to match
   */
  protected def removeAllFromScenes(key:String, any: Any): Unit = {
    for (scene <- _scenes.values) {
      if (scene.contains(any)) {
        scene.remove(key, any)
      }
    }
  }

  /*
   * Removes from the defined scene (using key)
   * If and only if both the object's key and value matches
   *@param sceneKey the key to find the scene
   *@param objKey the key to find the obj in the scene
   *@param any the value to match
   */
  protected def removeFromScene(sceneKey:String, objKey:String, any: Any): Any = {
    if (_scenes.contains(sceneKey) && _scenes(sceneKey).contains(objKey, any))
      return removeFromScene(sceneKey, objKey)
    null
  }

  protected def removeFromScene(sceneKey:String, objKey:String): Any = {
    if (_scenes.contains(sceneKey))
      return _scenes(sceneKey).remove(objKey)
    null
  }

  protected def removeFromData(key:String): Unit ={
    _datatable -= key
  }

  override def buildDataTable(): Map[String, Any] = {
    var datatable = super.buildDataTable()
    datatable = datatable ++ _datatable
    datatable
  }
}
