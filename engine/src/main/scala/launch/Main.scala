package launch

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.{Game, Gdx}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.badlogic.gdx.graphics.{Color, GL20}
import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}
import game.APage

class Main extends Game {
  private var _batch:SpriteBatch = _
  private var _font:BitmapFont = _
  private var _page:APage = _

  override def create(): Unit = {
    _batch = new SpriteBatch()
    _font = new BitmapFont()
    _font.setColor(Color.WHITE)
    _page = new APage("Demo Page")
    _page.init()
  }

  override def render(): Unit = {
    Gdx.gl.glClearColor(0, 0, 0, 0)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    _batch.begin()
    _page.run(_batch, _font)
    _batch.end()
  }

  override def dispose(): Unit = {

  }

}

object Main {
  def main(args: Array[String]): Unit = {
    val config:LwjglApplicationConfiguration = new LwjglApplicationConfiguration
    val main:Main = new Main
    var app:LwjglApplication = new LwjglApplication(main, config)
    var input = app.getInput
    while (!input.isKeyJustPressed(Keys.ESCAPE)) {
      if (input.isKeyJustPressed(Keys.LEFT)) {
        println("Previous")

      }
      else if (input.isKeyJustPressed(Keys.RIGHT)) {
        println("Next")
      }
      input = app.getInput
    }
    app.exit()
    println("Exiting...")
  }
}
