package game

import com.badlogic.gdx.{Gdx, Input, InputAdapter}
import com.badlogic.gdx.graphics.g2d.{BitmapFont, GlyphLayout, SpriteBatch}
import engine.objects.AActor
import scala.collection.mutable.ArrayBuffer

class APage(_name:String) extends AActor(_name) {
  name = _name
  private var _lines:ArrayBuffer[Line] = new ArrayBuffer[Line]()
  private var _currentIdx:Int = 0

  override def init(): Unit = {
    Gdx.input.setInputProcessor(new InputAdapter() {
      override def keyUp(keycode: Int): Boolean = {
        if (keycode == Input.Keys.RIGHT) {
          _currentIdx += 1
          if (_currentIdx > _lines.size) _currentIdx = _lines.size
          return true
        }
        else if (keycode == Input.Keys.LEFT) {
          _currentIdx -= 1
          if (_currentIdx < 0) _currentIdx = 0
          return true
        }
        return true
      }
    })

  }

  def run(batch: SpriteBatch, font: BitmapFont): Unit =
  {
    for (i <- 0 until _currentIdx) {
      font.draw(batch, _lines(i).line, 5, _lines(i).position, APage.MAX_LENGTH, -1, true)
    }
  }

  def add(line:String): Unit = {
    if (!willBeFull(line)) {
      if (_lines.size == 0) {
        _lines += new Line(line, APage.MAX_HEIGHT)
      }
      else {
        val lastIndex = _lines.size - 1

        var position = _lines(lastIndex).position
        val prevLineNumOfLines = findNumOfLines(_lines(lastIndex).line)
        if (prevLineNumOfLines > 1)
          position -= math.round(prevLineNumOfLines.asInstanceOf[Float] / 2) * 20
        position -= 20

        val lineAndPos = new Line(line, position)
        if (!_lines.contains(lineAndPos))
          _lines += lineAndPos
      }
    }
  }

  def willBeFull(line:String, font: BitmapFont = new BitmapFont()): Boolean = {
    var numOfLines = findNumOfLines(line, font)
    for (i <- 0 until _lines.size) {
      numOfLines += findNumOfLines(_lines(i).line, font)
    }
    return (numOfLines * 20 ) > APage.MAX_HEIGHT
  }

  def findNumOfLines(line:String, font: BitmapFont = new BitmapFont()): Int = {
    var width:Float = new GlyphLayout(font, line).width
    var numOfLines:Int = 0
    while (width > 0) {
      width -= APage.MAX_LENGTH
      numOfLines += 1
    }
    return numOfLines
  }

  private class Line(var line:String, var position:Float) {}
}

object APage {
  val MAX_HEIGHT:Float = 470
  val MIN_HEIGHT:Float = 20
  val X_POS:Float = 5

  val MAX_LENGTH:Float = 500
  val MIN_LENGTH:Float = 20

  def main(args: Array[String]): Unit = {
  }
}